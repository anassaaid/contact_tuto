import React, { Component  } from 'react';
import Select from 'react-select';
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      name: '',
      phoneNumber: '',
      messageType: '',
      message: '',

      savedName: '',
      savedPhoneNumber: '',
      savedMessageType: '',
      savedMessage: '',

      error: null,
      isLoaded: false,
      messageTypeOptions: []
    }

    this.loadMessageTypeOptions();
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange = (e) => {
    let newState = {}

    newState[e.target.name] = e.target.value

    this.setState(newState)
  }

  /**
   * Load MessageTypes from api and parse them for select options
   */
  loadMessageTypeOptions = () => {
    fetch("http://127.0.0.1:8000/api/type_messages")
      .then(res => res.json())
      .then(
        (result) => {
          var items = [];
          result['hydra:member'].forEach(element => {
            items.push({
              "label" : element.label,
              "value" : element.id
            });
          });
          
          this.setState({
            isLoaded: true,
            messageTypeOptions: items
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  clearForm = (formData) => {
    this.setState({
      name:'',
      phoneNumber:'',
      messageType:'',
      message:'',
      savedName:formData.name,
      savedPhoneNumber:'',
      savedMessageType:'',
      savedMessage:formData.message
    });
  }

  fillForm = () => {
    this.setState({
      name : this.state.savedName,
      message : this.state.message
    });
  }

  checkRequiredFields = (formData) => {
    if (formData.name.length < 1){
      alert("Lechamp nom et prénom est obligatoire");
      return false;
    }else if(formData.messageType === '/api/type_messages/'){
      alert('Le champ type est obligatoire');
      return false;
    }else if(formData.message.length < 1) { 
      alert('Veuillez remplir les chanps obligatoires!');
      return false;
    }
    return true;
  }

  callApi(formData, url,dataType,type,headers,data){
    $.ajax({
      url: url,
      dataType: dataType,
      type: type,
      headers: headers,
      data: data,
      success: function (data) {
        //  show success notification
        switch(formData.messageType){
          case "/api/type_messages/1" :  
            alert('Merci pour votre message, votre demande a été transmise au service RH.')
            break;
          case "/api/type_messages/2" : 
            alert('Merci pour votre message, votre demande a été transmise au support client.')
            break;  
          case "/api/type_messages/3" : 
            alert('Merci pour votre message, votre demande a été transmise à l’équipe commercial.')
            break;
          default : 
            alert('...')
            break;    
        }  

        //  clear data
        this.clearForm(formData);      
      }.bind(this),
      error: function (xhr, status, err) {
        var response = JSON.parse(xhr.responseText);
        if(typeof response.violations !== 'undefined'){// get error message from api
          alert(response.violations[0].message);
        }else{
          alert('Une erreur c\'est produite!');
        }        
      }
    })
  }

  /**
   *  Action on submit
   */
  handleSubmit = (e, message) => {
    e.preventDefault()

    let formData = {
      "name": this.state.name,
      "phoneNumber": this.state.phoneNumber,
      "messageType": "/api/type_messages/" + this.state.messageType,
      "message": this.state.message
    }

    //  Required fields
    var pass = this.checkRequiredFields(formData);
    if(!pass) return false;

    //  persist data to api
    this.callApi(formData,'http://127.0.0.1:8000/api/contacts',
      'json',
      'POST',
      {
        "accept": "application/ld+json",
        "Content-Type": "application/ld+json"
      },
      JSON.stringify(formData)
    );  

  }

  render() {
    return (
      <div className="container">
      <form className='react-form' onSubmit={this.handleSubmit}>
        <h1>Contact : </h1>

        <div className="form-group">
            <label htmlFor="name">Nom et prénom :</label>
            <input type="text" 
                   className="form-control" 
                   id="name" 
                   name="name" 
                   required  
                   onChange={this.handleChange} 
                   value={this.state.name} />
        </div>

        <div className="form-group">
            <label htmlFor="name">Téléphone :</label>
            <input type='tel' 
                   className='form-control' 
                   id='phoneNumber' 
                   name='phoneNumber' 
                   onChange={this.handleChange} 
                   value={this.state.phoneNumber} />
        </div>

        <div className="form-group">
            <label htmlFor="messageType">Type :</label>
            <Select
              required 
              value={this.state.messageType}
              options={this.state.messageTypeOptions}
              onChange={opt => this.setState({messageType : opt.value}) } />
        </div>

        <div className="form-group">
            <label htmlFor="message">Message :</label>
            <textarea id='formMessage' 
                      className='form-control' 
                      name='message' 
                      value={this.state.message}
                      required onChange={this.handleChange}></textarea>
        </div>

        <div className='form-group'>
          <input id='formButton' className='btn btn-primary' type='submit' placeholder='Envoyer' />
          &nbsp;<input id='formButton' 
                       className='btn' 
                       type='button' 
                       onClick={this.fillForm}
                       value="Effectuer une nouvelle demande" 
                       placeholder='Effectuer une nouvelle demande' />
          
        </div>
      </form>

      </div>

      

    )
  }

}

export default App;
