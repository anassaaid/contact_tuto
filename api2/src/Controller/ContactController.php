<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contact;
use App\Form\ContactType;

use App\Service\FormHandler;

class ContactController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/contact", name="post_contact", methods={"POST"})
     */
    public function post(Request $request, FormHandler $formHandler)
    {

        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(ContactType::class, new Contact());

        $form->submit($data);
        
        if (false === $form->isValid()) {

            $errors = $formHandler->getErrorsFromForm($form);
            
            return new JsonResponse(
                [
                    'status' => 'Données invalides',
                    'errors' => $errors
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $this->entityManager->persist($form->getData());
        $this->entityManager->flush();

        return new JsonResponse(
            [
                'status' => 'ok'
            ],
            JsonResponse::HTTP_CREATED
        );     
    }

}
