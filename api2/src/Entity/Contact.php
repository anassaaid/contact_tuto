<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Length(
     *      min = 5,
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @Assert\NotBlank(
     *      message = "Le champ message est obligatoire!"
     * )
     * 
     * @Assert\Length(
     *      max = 10,
     *      maxMessage = "Your message cannot be longer than {{ limit }} characters"
     * )
     * 
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @Assert\NotBlank(
     *      message = "Le champ type est obligatoire!"
     * )
     * 
     * @ORM\Column(type="string", length=255)
     */
    private $type_message;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getTypeMessage(): ?string
    {
        return $this->type_message;
    }

    public function setTypeMessage(string $type_message): self
    {
        $this->type_message = $type_message;

        return $this;
    }
}
